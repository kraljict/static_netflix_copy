This is a static copy of Netflix's landing page with only using HTML5 and CSS3.

Here is a live demo: https://tomislavkraljic.github.io/Static_Netflix_Copy/

<img src="./assets/Netflix_Copy.png">